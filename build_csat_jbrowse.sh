#!/bin/bash


PREFIX1="../data/csat/assemblies"
PREFIX2="../data/csat/pairwise_alignments"

jbrowse create jbrowsers/csat/
cd jbrowsers/csat/

# adds each assembly
parallel -j 1 \
    "jbrowse add-assembly -n {1} -l inPlace $PREFIX1/{1}/{1}.softmasked.fasta.gz" \
    :::: ../../metadata/bases

# adds each helixer gene gff
parallel -j 1 \
    "jbrowse add-track -a {1} -l inPlace  $PREFIX1/{1}/genes_v2/{1}.v2.gff3.gz" \
    :::: ../../metadata/bases

# adds each older tsebra gff
parallel -j 1 \
    "jbrowse add-track -a {1} -l inPlace  $PREFIX1/{1}/genes_v1/{1}.v1.all.sorted.gff3.gz" \
    :::: ../../metadata/bases

# indexes the gffs so that genes are searchable
jbrowse text-index  --prefixSize=40

# adds each pairwise alignment track
parallel -j 1 --colsep ' ' \
	"jbrowse add-track $PREFIX2/{1}.vs.{2}.filt_10kb.paf -t=SyntenyTrack -l inPlace -a {1},{2}" \
    :::: ../../metadata/pairwise_aligns

# adds graph pangenome vcfs
parallel -j 1 --colsep ' ' \
    "jbrowse add-track -a {1} -l inPlace {2}" \
    :::: ../../metadata/vcfs

#random tracks
jbrowse add-track -a BH4 -l inPlace ../data/csat/assemblies/BH4/other_tracks/BH4.CannabinoidFree_specific_kmers.bw
