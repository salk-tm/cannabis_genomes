git repo for cannabis pangenome sharing

This repo contains two apps, one for making cannabis genome files available and one for generating a jbrowse instance. Whether working with either, the following quick start should work...

### Quick Start
```
# creates an environment with most of the needed tools
conda create -n jbrowse -y -c conda-forge -c bioconda \
    nodejs yarn git awscli samtools tabix genometools-genometools
conda activate jbrowse
npm install -g serve @jbrowse/cli
npm install -g http-server

# clone this repo using ssh or https
git clone git@gitlab.com:salk-tm/cannabis_genomes.git
git clone https://gitlab.com/salk-tm/cannabis_genomes.git
cd cannabis_genomes

# s3fs must be installed seperately and is already installed on seabiscuit
# mounts data files form salk-tm-web as needed for apps
mkdir -p salk-tm-web && s3fs salk-tm-web salk-tm-web -o ro


# The following link should already exist in your git repo
# ln -s ../salk-tm-web/jbrowsers/data/ jbrowsers/data

# builds the jbrowse instance
./build_csat_jbrowse.sh

# alternatively, pull the current instance
aws s3 cp s3://salk-tm-web/jbrowsers/csat/ jbrowsers/csat/ --recursive

# at this point you can make whatever changes you want in cannabis_genomes or jbrowsers/csat/
# if you update jbrowsers/csat, make sure you update the build_csat_jbrowse.sh script

# launches a local server for testing apps
http-server ./

# to unmount salk-tm-web when finished with dev
umount salk-tm-web

# push changes to jbrowsers back to salk-tm-web
aws s3 sync jbrowsers/csat/ s3://salk-tm-web/jbrowsers/csat/

# push changes to cannabis_genomes (requires package from salk-tm-web-infrastructure)
s3deploy.py cannabis_genomes/

# run the following to force cloudfront cache to update for jbrowsers
aws cloudfront create-invalidation --distribution-id E3OK3GPB6OYBIR --paths "/jbrowsers/csat/*"

# run the following to force cloudfront cache to update for cannabis_genomes
aws cloudfront create-invalidation --distribution-id E3OK3GPB6OYBIR --paths "/resources/cannabis_genomes/*"
```

### Using Visual Studio Code to dev on seabiscuit
Basically, you do all of the above, but you need to do it through vscode. This requires using the 'remote-ssh' package to allow for vscode to remote into seabiscuit. And make sure you are using a vscode terminal within that remote session.

Generic steps:

1. install visual studio code
2. install remote-ssh extension within vs code
3. open remote-explorer and start a new ssh connection to <username>@seabiscuit.salk.edu
4. open a directory location where you want to work
5. open a terminal 
6. Following something like the quick start method above. When you run http-server, vscode should automatically port forward localhost to your computer and prompt you to open the link in your browser.

